package com.module1.introduction;

import com.google.common.collect.Iterables;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.List;

public class MainPairIDDs {
    static void logInfo(Object o) {
        Logger.getLogger("com.basic.rdd").info("<<<< " + o + " >>>>");
    }
    public static void main(String[] args) {
        /* SETUP */
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        SparkConf conf = new SparkConf().setAppName("appName").setMaster("local[*]");
        JavaSparkContext sc = new JavaSparkContext(conf);

        List<String> stringList = new ArrayList<>();
        stringList.add("WARN: Tuesday 4 september 0405");
        stringList.add("ERROR: Tuesday 4 september 0408");
        stringList.add("FATAL: Wednesday 5 september 1632");
        stringList.add("ERROR: friday 7 september 1854");
        stringList.add("WARN: saturday 8 september 1942");

        /* SHORT */
        sc.parallelize(stringList)
                .mapToPair(s -> new Tuple2<>(s.split(":")[0], 1L))   // map the value of the set to 1
                .reduceByKey(Long::sum)                                     // (sum values)/key (# of elements)
                .foreach(MainPairIDDs::logInfo);

        /* GROUP-BY (VERY EXPENSIVE, DON'T DO THIS ON A CLUSTER RDD -> WILL CAUSE CRASHES) */
        sc.parallelize(stringList)
                .mapToPair(s -> new Tuple2<>(s.split(":")[0], 1L))        // map the value of the set to 1
                .groupByKey()
                .foreach(tuple -> logInfo(tuple._1 +" has size "+ Iterables.size(tuple._2)));

        sc.close();
    }
}
