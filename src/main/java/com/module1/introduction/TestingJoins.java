package com.module1.introduction;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.Optional;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.List;


public class TestingJoins {
    static String LINEBREAK = "-------------------------------------------------------------";
    public static void main(String[] args) {
        System.setProperty("hadoop.home.dir", "c:/hadoop");
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        SparkConf sparkConfig = new SparkConf().setAppName("appName").setMaster("local[*]");
        JavaSparkContext sc = new JavaSparkContext(sparkConfig);

        List<Tuple2<Integer, Integer>> visitsTuple2List = new ArrayList<>();
            visitsTuple2List.add(new Tuple2<>(4,18));
            visitsTuple2List.add(new Tuple2<>(6,4));
            visitsTuple2List.add(new Tuple2<>(10,9));
        List<Tuple2<Integer, String>> userList = new ArrayList<>();
            userList.add(new Tuple2<>(1, "John"));
            userList.add(new Tuple2<>(2, "Bob"));
            userList.add(new Tuple2<>(3, "Alan"));
            userList.add(new Tuple2<>(4, "Doris"));
            userList.add(new Tuple2<>(5, "Marybelle"));
            userList.add(new Tuple2<>(6, "Raquel"));

        JavaPairRDD<Integer, Integer> visitsPairRDD = sc.parallelizePairs(visitsTuple2List);
        JavaPairRDD<Integer, String> usersPairRDD = sc.parallelizePairs(userList);




        logInfo(LINEBREAK);
        usersPairRDD.leftOuterJoin(visitsPairRDD)
                .foreach(e -> logInfo("(visitorID = numberOfVisits) " + e._1 + " = " + e._2._2.orElse(0)));

        logInfo(LINEBREAK);
        JavaPairRDD<Integer, Tuple2<Optional<Integer>, String>> helpRef = visitsPairRDD.rightOuterJoin(usersPairRDD);
        helpRef.foreach(row -> logInfo(row._2._2 + " has "+ row._2._1.orElse(0) +" visits"));

        logInfo(LINEBREAK);
        JavaPairRDD<Tuple2<Integer, Integer>, Tuple2<Integer, String>> cartesian = visitsPairRDD.cartesian(usersPairRDD);
        cartesian.foreach(TestingJoins::logInfo);

    }
    static void logInfo(Object o) { Logger.getLogger("com.joins").info("\t*** MY_LOG ***\t|\t"+o); }
}
