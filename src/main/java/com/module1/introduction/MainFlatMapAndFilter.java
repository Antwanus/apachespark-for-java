package com.module1.introduction;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainFlatMapAndFilter {
    static void logInfo(Object o) {
        Logger.getLogger("com.basic.rdd").info("<<<< " + o + " >>>>");
    }
    public static void main(String[] args) {
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        SparkConf conf = new SparkConf().setAppName("appName").setMaster("local[*]");
        JavaSparkContext sc = new JavaSparkContext(conf);

        List<String> stringList = new ArrayList<>();
        stringList.add("WARN: Tuesday 4 september 0405");
        stringList.add("ERROR: Tuesday 4 september 0408");
        stringList.add("FATAL: Wednesday 5 september 1632");
        stringList.add("ERROR: friday 7 september 1854");
        stringList.add("WARN: saturday 8 september 1942");

        sc.parallelize(stringList)
                .flatMap(wordsInOneString ->
                        Arrays.asList(wordsInOneString.split(" ")).iterator()
                )
                .filter(word -> word.length() > 1)
                .foreach(MainFlatMapAndFilter::logInfo);

        sc.close();
    }
}
