package com.module1.introduction;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.util.Arrays;
import java.util.Scanner;

public class MainReadFromDisk {
    public static void main(String[] args) {
        System.setProperty("hadoop.home.dir", "c:/hadoop");
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        SparkConf conf = new SparkConf().setAppName("appName").setMaster("local[*]");
        JavaSparkContext sc = new JavaSparkContext(conf);

        JavaRDD<String> inputSourceRDD = sc.textFile("src/main/resources/subtitles/input-spring.txt");

        inputSourceRDD
                .map(line -> line.replaceAll("[^a-zA-Z\\s]", "").toLowerCase())
                .flatMap(line -> Arrays.asList(line.split(" ")).iterator())
                .filter(Util::isNotBoring)
                .filter(line -> !line.isEmpty())
                .mapToPair(word -> new Tuple2<>(word, 1L))
                .reduceByKey(Long::sum)
                .mapToPair(tuple -> new Tuple2<>(tuple._2, tuple._1))
                .sortByKey(false) // default (true) = asc
        /** up to now we were coding 'transformations' (lazy execution), Spark is building the 'execution plan'
         *  .take(10) is an 'action' -> spark starts calculating data because it has to return something
         *  Spark still needs to compute the inputSourceRDD (line 20ish)
         */
                .take(10)
            .forEach(MainReadFromDisk::logInfo);

        Scanner scanner = new Scanner(System.in); // little hack to keep the app running for monitoring in SparkUI
        scanner.nextLine();

        sc.close();
    }
    static void logInfo(Object o) { Logger.getLogger("com.basic.rdd").info(o); }
}

