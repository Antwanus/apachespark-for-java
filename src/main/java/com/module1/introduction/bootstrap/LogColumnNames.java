package com.module1.introduction.bootstrap;

public enum LogColumnNames {
    MONTH, DATETIME, MONTH_NUMBER, LEVEL
}
