package com.module1.introduction;

import java.util.Optional;
import java.util.Scanner;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.Partitioner;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.storage.StorageLevel;
import org.spark_project.guava.collect.Iterables;

import scala.Tuple2;

public class PartitionTesting {
	public static void main(String[] args) {
		System.setProperty("hadoop.home.dir", "c:/hadoop");
//		Logger.getLogger("org.apache").setLevel(Level.WARN);
		
		SparkConf conf = new SparkConf().setAppName("startingSpark").setMaster("local[*]");
		JavaSparkContext sc = new JavaSparkContext(conf);
		
		JavaRDD<String> initialRdd = sc.textFile("src/main/resources/bigLog.txt");

		logInfo("Initial RDD Partition Size: " + initialRdd.getNumPartitions());
		
		JavaPairRDD<String, String> warningsAgainstDate = initialRdd.mapToPair( inputLine -> {
			String[] cols = inputLine.split(":");
			String level = cols[0];
			String date = cols[1];
			return new Tuple2<>(level, date);
		});
		
		logInfo("After a NARROW transformation we have " + warningsAgainstDate.getNumPartitions() + " parts");
		
		// Now we're going to do a "wide" transformation
		JavaPairRDD<String, Iterable<String>> results = warningsAgainstDate.groupByKey();
		
		results = results.persist(StorageLevel.MEMORY_AND_DISK());
		
		logInfo(results.getNumPartitions() + " partitions after the WIDE transformation");
		
		results.foreach(it -> logInfo("key " + it._1 + " has " + Iterables.size(it._2) + " elements"));
		
		logInfo(results.count());
		
		Scanner scanner = new Scanner(System.in);
		scanner.nextLine();
		sc.close();
	}
	static void logInfo(Object object) { Logger.getLogger("com.joins").info("\t*** MY_LOG ***\t|\t"+object); }

}
