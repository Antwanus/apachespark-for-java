package com.module1.introduction;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.List;

public class Main {
    static void logInfo(Object o) {
        Logger.getLogger("com.basic.rdd").info("<<<< " + o + " >>>>");
    }
    public static void main(String[] args) {
        /* SETUP */
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        SparkConf conf = new SparkConf().setAppName("appName").setMaster("local[*]");
        JavaSparkContext sc = new JavaSparkContext(conf);

        List<Integer> integerList = new ArrayList<>();
        integerList.add(1);
        integerList.add(2);
        integerList.add(3);
        integerList.add(4);
        integerList.add(5);
        integerList.add(6);
        integerList.add(7);

        JavaRDD<Integer> integerListRDD = sc.parallelize(integerList);

        // we need to combine intList & mappedSqrt
//        JavaRDD<Double> mappedSqrtIntegerRDD = sourceIntegerListRDD.map(Math::sqrt);
        // created wrapper class, so data is on same 'row' in the RDD
        JavaRDD<IntegerWithSquareRoot> mappedSqrtIntegerRDD = integerListRDD.map(IntegerWithSquareRoot::new);

        // create tuple2 (like a map, but keys are NOT unique) instead of a class
        JavaRDD<Tuple2<Integer, Double>> tuple2RDD = integerListRDD.map(v1 -> new Tuple2<>(v1, Math.sqrt(v1)));

        tuple2RDD.foreach(Main::logInfo);
        sc.close();
    }
}
