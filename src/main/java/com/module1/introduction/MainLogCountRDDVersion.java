package com.module1.introduction;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;

import scala.Tuple2;

import static com.module1.introduction.SerializableComparator.*;


public class MainLogCountRDDVersion {
    static void logInfo(Object o) { Logger.getLogger("com.sparksql").info("*** My Log ***\t-->\t" + o); }
    public static void main(String[] args) {
        System.setProperty("hadoop.home.dir", "c:/hadoop");
        Logger.getLogger("org.apache").setLevel(Level.WARN);

        JavaSparkContext sc = new JavaSparkContext(
                new SparkConf().setMaster("local[*]").setAppName("misterAppName")
        );
        JavaRDD<String> javaRDD = sc.textFile("src/main/resources/bigLog.txt");
        /**      javaRDD.take(3).foreach(MainLogCountRDDVersion::logInfo);
         * 21/03/18 18:12:03 INFO com.sparksql: *** My Log ***	-->	level,datetime
         * 21/03/18 18:12:03 INFO com.sparksql: *** My Log ***	-->	DEBUG,2015-2-6 16:24:07
         * 21/03/18 18:12:03 INFO com.sparksql: *** My Log ***	-->	WARN,2016-7-26 18:54:43
         */
        Comparator<String> comparator = serialize( (levelMonthTuple2Val1,levelMonthTuple2Val2) -> {
            String monthA = levelMonthTuple2Val1.split(":")[1];
            String monthB = levelMonthTuple2Val2.split(":")[1];
            return monthToMonthnum(monthA) - monthToMonthnum(monthB);
        });
        javaRDD
                .filter(row -> !row.startsWith("level,datetime"))   //remove header
                .mapToPair(s -> {
                    String[] csvFields = s.split(",");
                    String level = csvFields[0];
                    String datetime = csvFields[1];
                    String month = rawDatetimeToMonth(datetime);
                    String key = level +":"+ month;
                    return new Tuple2<>(key, 1L);
                }) // ( (LEVEL,DATETIME), 1 )
                .reduceByKey(Long::sum)
                .sortByKey().sortByKey(comparator)
            .take(100)
            .forEach(MainLogCountRDDVersion::logInfo);


        sc.close();
    }

    private static String rawDatetimeToMonth(String raw) {
        SimpleDateFormat rawFmt = new SimpleDateFormat("yyyy-M-d hh:mm:ss");
        SimpleDateFormat requiredFmt = new SimpleDateFormat("MMMM");
        Date results;
        try
        {
            results = rawFmt.parse(raw);
            String month = requiredFmt.format(results);
            return month;
        }
        catch (ParseException e)
        {
            throw new RuntimeException(e);
        }
    }
    private static int monthToMonthnum(String month) {
        SimpleDateFormat rawFmt = new SimpleDateFormat("MMMM");
        SimpleDateFormat requiredFmt = new SimpleDateFormat("M");
        Date results;
        try {
            results = rawFmt.parse(month);
            int monthNum = new Integer(requiredFmt.format(results));
            return monthNum;
        }
        catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

}
