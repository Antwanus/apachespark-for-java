package com.module2.sparkSQL;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;

import java.util.Scanner;

import static org.apache.spark.sql.functions.*;


public class MainUserDefinedFunctions {
    static void logInfo(Object o) { Logger.getLogger("com.sparksql").info("*** My Log ***\t-->\t" + o); }
    public static void main(String[] args) {
        System.setProperty("hadoop.home.dir", "c:/hadoop");
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        SparkSession sparkSession = SparkSession.builder()
                .appName("UDF_main").master("local[*]")
                .config("spark.sql.warehouse.dir", "file:///c:/tmp/")
                .getOrCreate();
        Dataset<Row> dataset = sparkSession.read()
                .option("header", true)
                .csv("src/main/resources/exams/students.csv");

        /** dataset.show();
         * +----------+--------------+----------+----+-------+-----+-----+
         * |student_id|exam_center_id|   subject|year|quarter|score|grade|
         * +----------+--------------+----------+----+-------+-----+-----+
         * |         1|             1|      Math|2005|      1|   41|    D|
         * |         1|             1|   Spanish|2005|      1|   51|    C|
         * |         1|             1|    German|2005|      1|   39|    D|
         * ...
         */
        sparkSession.udf().register(
                "hasPassedByGrade",
                (String grade) -> {
                    if (grade.startsWith("A") || grade.startsWith("B")){
                        return true;
                    } else if (grade.startsWith("C")) {
                        return null;
                    } else return false;
                },
                DataTypes.BooleanType
        );
        sparkSession.udf().register(
                "hasPassedBySubject",
                (String subject, String grade) -> {
                    if (subject.equalsIgnoreCase("biology")) {
                        if (grade.startsWith("A") || grade.startsWith("B") || grade.startsWith("C")) return true;
                        else if (grade.startsWith("D")) return null;
                        else return false;
                    } else return null;
                },
                DataTypes.BooleanType
        );

        dataset.withColumn(
                "pass",
//                lit("YES")                                           // fills every pass_record with "YES"
//                lit( col("grade").equalTo("A+") )
                callUDF("hasPassedBySubject", col("subject"), col("grade"))
        ).show();
        /**
         * +----------+--------------+----------+----+-------+-----+-----+-----+
         * |student_id|exam_center_id|   subject|year|quarter|score|grade| pass|
         * +----------+--------------+----------+----+-------+-----+-----+-----+
         * |         2|             1| Chemistry|2005|      1|   26|    E|false|
         * |         2|             1|   Biology|2005|      1|   90|   A+| true|
         * |         2|             1|Modern Art|2005|      1|   55|    C|false|
         * ...
         * */

        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        sparkSession.close();
    }
}
