package com.module2.sparkSQL;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;

import static org.apache.spark.sql.functions.*;

public class MainFilter {
    static void logInfo(Object o) { Logger.getLogger("com.sparksql").info("*** My Log ***\t-->\t" + o); }
    public static void main(String[] args) {
        System.setProperty("hadoop.home.dir", "c:/hadoop");
        Logger.getLogger("org.apache").setLevel(Level.WARN);

        SparkSession sparkSession = SparkSession.builder()
                .appName("appName").master("local[*]")
                .config("spark.sql.warehouse.dir", "file:///c:/tmp/")   // requires for Windows
                .getOrCreate();

        Dataset<Row> studentsCsvDataSet = sparkSession.read()
                .option("header", true) // first line in the CSV file is header values/names
                .csv("src/main/resources/exams/students.csv");

        logInfo("dataset.show()");
        studentsCsvDataSet.show();
        logInfo("(Dataset<Row>) csvDataSet.count() => " + studentsCsvDataSet.count());

        /** FILTER BY EXPRESSION */
//        Dataset<Row> filteredDataSet = studentsCsvDataSet.filter("subject = 'Modern Art' AND year >= 2007");
//        logInfo("filteredDataset.show()");
//        logInfo("(Dataset<Row>) csvDataSet.count() => " + filteredDataSet.count());
//        filteredDataSet.show();
        /** FILTER BY LAMBDA */
//        Dataset<Row> modernArtResult = studentsCsvDataSet.filter(
//                (Row row) ->
//                    row.getAs("subject").equals("Modern Art")
//                    && Integer.parseInt(row.getAs("year")) >= 2007
//        );
//        modernArtResult.show();
        /** FILTER BY COLUMNS */
        Column subjectColumn = studentsCsvDataSet.col("subject").cast(DataTypes.StringType);
        Column yearColumn = studentsCsvDataSet.col("year").cast(DataTypes.IntegerType);
        Column examCenterIdColumn = studentsCsvDataSet.col("exam_center_id").cast(DataTypes.IntegerType);
        Dataset<Row> modernArtResult = studentsCsvDataSet
                 .filter(   subjectColumn.equalTo("Modern Art")
                    .and(   yearColumn.geq(2007))
                    .and(   examCenterIdColumn.equalTo(2))
        );
        modernArtResult.show();

        // nice...
        Column yearCol = col("year");
        Column subjectCol = column("subject");
        studentsCsvDataSet
                 .filter(   subjectCol.equalTo("Modern Art")
                    .and(   yearCol.geq(2007))
                    .and(   col("exam_center_id").equalTo(2))
                    .and(   functions.column("student_id").geq(42840))
        ).show();


        /** BASIC SELECT, TYPE CONVERSION*/
//        Row firstRow = studentsCsvDataSet.first();
//        logInfo("firstRow = csvDataSet.first() => " + firstRow);
//        String subject = firstRow.getString(2);
//        logInfo("firstRow.getString(2) => " + subject);
//        String subjectByFieldName = firstRow.getAs("subject").toString();
//        logInfo("firstRow.getAs(fieldName: \"subject\").toString() => " + subjectByFieldName + " --> will attempt type conversion");
////        int year = firstRow.getAs("year"); // runtime ClassCastException (csv-file returns String)
//        int year = Integer.parseInt(firstRow.getAs("year"));
//        logInfo("int year = Integer.parseInt(firstRow.getAs(\"year\")) => " + year);


    }
}
