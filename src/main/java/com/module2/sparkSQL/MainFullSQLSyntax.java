package com.module2.sparkSQL;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class MainFullSQLSyntax {
    static void logInfo(Object o) { Logger.getLogger("com.sparksql").info("*** My Log ***\t-->\t" + o); }
    public static void main(String[] args) {
        System.setProperty("hadoop.home.dir", "c:/hadoop");
        Logger.getLogger("org.apache").setLevel(Level.WARN);

        SparkSession sparkSession = SparkSession.builder()
                .appName("appName").master("local[*]")
                .config("spark.sql.warehouse.dir", "file:///c:/tmp/")   // requires for Windows
                .getOrCreate();
        Dataset<Row> dataset = sparkSession.read()
                .option("header", true) // first line in the CSV file is header values/names
                .csv("src/main/resources/exams/students.csv");

        dataset.filter("subject = 'Modern Art' AND year >= 2007").show();

        dataset.createOrReplaceTempView("my_students_table");
        Dataset<Row> rowDataset = sparkSession.sql(
//                        "SELECT score, year "
//                        "SELECT max(score)"
//                        "SELECT avg(score)"
                        "SELECT distinct(year) "
                                +"FROM my_students_table "
//                          +"WHERE subject = 'French'"
                                +"order by year desc"
        );

        rowDataset.show();

        sparkSession.close();
    }
}
