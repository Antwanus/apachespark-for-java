package com.module2.sparkSQL;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;

import static org.apache.spark.sql.functions.*;


public class MainAggregation {
    public static void main(String[] args) {
        System.setProperty("hadoop.home.dir", "c:/hadoop");     // targets dummy hadoop dir (hack for windows)
        Logger.getLogger("org.apache").setLevel(Level.WARN);    // reduce buzz

        SparkSession sparkSession = SparkSession.builder()
                .appName("testingSQL").master("local[*]")
                .config("spark.sql.warehouse.dir", "file:///c:/tmp/")
                .getOrCreate();

        Dataset<Row> dataset = sparkSession.read()
                    .option("header", true)
/** inferSchema (default false): infers the input schema automatically from data. It requires one extra pass over the data.
 *  -> This could be a very expensive operation depending on parameters of dataset */
//                    .option("inferSchema", true)              // auto-casts data
                .csv("src/main/resources/exams/students.csv");
        /** initial dataset
         * +----------+--------------+----------+----+-------+-----+-----+
         * |student_id|exam_center_id|   subject|year|quarter|score|grade|
         * +----------+--------------+----------+----+-------+-----+-----+
         * |         1|             1|      Math|2005|      1|   41|    D|
         * |         1|             1|   Spanish|2005|      1|   51|    C|
         * ...
         * */

        Column scoreAsInt = dataset.col("score"); //.cast(DataTypes.IntegerType); --> .agg() seems to auto-cast
        dataset
                .groupBy("subject")
                    .agg(
                        max(scoreAsInt).alias("max_score"),
                        avg(scoreAsInt).alias("avg_score"),
                        min(scoreAsInt).alias("min_score")
                    )
            .show(100);

        sparkSession.close();
    }
}
