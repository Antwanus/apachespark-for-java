package com.module2.sparkSQL;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class Main {
    static void logInfo(Object o) { Logger.getLogger("com.sparksql").info("*** My Log ***\t-->\t" + o); }
    public static void main(String[] args) {
        System.setProperty("hadoop.home.dir", "c:/hadoop");
        Logger.getLogger("org.apache").setLevel(Level.WARN);

        SparkSession sparkSession = SparkSession.builder()
                                    .appName("appName").master("local[*]")
                                    .config("spark.sql.warehouse.dir", "file:///c:/tmp/")   // requires for Windows
                                    .getOrCreate();

        Dataset<Row> studentsCsvDataSet = sparkSession.read()
                            .option("header", true) // first line in the CSV file is header values/names
                            .csv("src/main/resources/exams/students.csv");

        logInfo("dataset.show()");
        studentsCsvDataSet.show();
        logInfo("(Dataset<Row>) csvDataSet.count() => " + studentsCsvDataSet.count());
        Row firstRow = studentsCsvDataSet.first();
        logInfo("firstRow = csvDataSet.first() => " + firstRow);
        String subject = firstRow.getString(2);
        logInfo("firstRow.getString(2) => "+subject);
        String subjectByFieldName = firstRow.getAs("subject").toString();
        logInfo("firstRow.getAs(fieldName: \"subject\").toString() => " + subjectByFieldName + " --> will attempt type conversion");
//        int year = firstRow.getAs("year"); // runtime ClassCastException (csv-file returns String)
        int year = Integer.parseInt(firstRow.getAs("year"));
        logInfo("int year = Integer.parseInt(firstRow.getAs(\"year\")) => " +year);


        sparkSession.close();
    }
}
