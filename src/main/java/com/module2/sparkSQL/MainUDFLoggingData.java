package com.module2.sparkSQL;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MainUDFLoggingData {
    static void logInfo(Object o) {
        Logger.getLogger("com.sparksql").info("*** My Log ***\t-->\t" + o);
    }

    private static final String[] months = new String[]{
            "January","February","March","April","Mai","June","JUL","August",
            "September","October","November","December"
    };
    public static void main(String[] args) {
        System.setProperty("hadoop.home.dir", "c:/hadoop");
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        SparkSession sparkSession = SparkSession.builder()
                .appName("UDF_main").master("local[*]")
                .config("spark.sql.warehouse.dir", "file:///c:/tmp/")
                .getOrCreate();
        sparkSession.conf().set("spark.sql.shuffle.partitions", 24); // default 200 => a lot of idle tasks/threads

        Dataset<Row> dataset = sparkSession.read()
                .option("header", true)
                .csv("src/main/resources/bigLog.txt");
        /** dataset.show();
         * +-----+-------------------+
         * |level|           datetime|
         * +-----+-------------------+
         * |DEBUG|  2015-2-6 16:24:07|
         * | WARN| 2016-7-26 18:54:43|
         * | INFO|2012-10-18 14:35:19|
         * ... */
        SimpleDateFormat inFormat = new SimpleDateFormat("MMMM");
        SimpleDateFormat outFormat = new SimpleDateFormat("M");

        sparkSession.udf().register("functionName",
                (String month) -> {
                    Date date = inFormat.parse(month);
                    return Integer.parseInt(outFormat.format(date));
                },
                DataTypes.IntegerType
        );
        dataset.createOrReplaceTempView("logging_table");
        Dataset<Row> result = sparkSession.sql(
                "SELECT level, " +
                                "date_format(datetime, 'MMMM') AS month, " +
                                "count(1) AS total " +
                              ", functionName(date_format(datetime, 'MMMM')) AS monthnum " +
                        "FROM logging_table " +
                        "GROUP BY   level, " +
                                    "month " +
//                        "ORDER BY   cast(first(date_format(datetime, 'M')) AS int)," +
                        "ORDER BY   cast(monthnum as int), " +
//                        "ORDER BY   functionName(month), " +
                                    "level "
        );
        result.show(100);

//        Scanner scanner = new Scanner(System.in);
//        scanner.nextLine();
        result.explain();



        sparkSession.close();
    }
}
