package com.module2.sparkSQL;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.Scanner;

public class MainGroupingAndOrdering {
        static void logInfo(Object o) { Logger.getLogger("com.sparksql").info("*** My Log ***\t-->\t" + o); }
        public static void main(String[] args) {
            System.setProperty("hadoop.home.dir", "c:/hadoop");
            Logger.getLogger("org.apache").setLevel(Level.WARN);

            SparkSession sparkSession = SparkSession.builder()
                    .appName("appName").master("local[*]")
                    .config("spark.sql.warehouse.dir", "file:///c:/tmp/")   // required for Windows
                    .getOrCreate();

            Dataset<Row> dataset = sparkSession.read()
                    .option("header", true).csv("src/main/resources/bigLog.txt");               // JobId_0: 0.3s

            dataset.createOrReplaceTempView("logging_table");
            Dataset<Row> result = sparkSession.sql(
                    "SELECT level, date_format(datetime, 'MMMM') AS month, count(1) AS counter " +
                            "FROM logging_table " +
                            "GROUP BY level, month " +
                            "ORDER BY cast(first(date_format(datetime, 'M')) AS int), counter DESC "
            );

            result.show(100);                                                                   //JobId_1: 8s

            Scanner scanner = new Scanner(System.in);
            scanner.nextLine();
            sparkSession.close();
        }
}

