package com.module2.sparkSQL;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/** https://spark.apache.org/docs/2.4.7/api/sql/index.html */
public class MainInMemoryData {
    static void logInfo(Object o) { Logger.getLogger("com.sparksql").info("*** My Log ***\t-->\t" + o); }
    public static void main(String[] args) {
        System.setProperty("hadoop.home.dir", "c:/hadoop");
        Logger.getLogger("org.apache").setLevel(Level.WARN);

        SparkSession sparkSession = SparkSession.builder()
                .appName("appName").master("local[*]")
                .config("spark.sql.warehouse.dir", "file:///c:/tmp/")   // required for Windows
                .getOrCreate();

        StructField[] structFields = new StructField[] {    // table header
                new StructField("level", DataTypes.StringType, false, Metadata.empty()),
                new StructField("datetime", DataTypes.StringType, false, Metadata.empty())
        };
        List<Row> inMemory = new ArrayList<>();
        inMemory.add(RowFactory.create("WARN", "2016-12-31 04:19:32"));
        inMemory.add(RowFactory.create("FATAL", "2016-12-31 03:22:34"));
        inMemory.add(RowFactory.create("WARN", "2016-12-31 03:21:21"));
        inMemory.add(RowFactory.create("INFO", "2015-4-21 14:32:21"));
        inMemory.add(RowFactory.create("FATAL","2015-4-21 19:23:20"));
        StructType schema = new StructType(structFields);
        Dataset<Row> datasetFrame = sparkSession.createDataFrame(inMemory, schema);

        datasetFrame.createOrReplaceTempView("logging_table");
        // because we used "grouped by" it demands a function => count()
        Dataset<Row> sql = sparkSession.sql("SELECT level, count(datetime) FROM logging_table group by level");
        sql.show();


        sparkSession.close();
    }
}
