package com.module2.sparkSQL;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class MainDateFormatting {
        static void logInfo(Object o) { Logger.getLogger("com.sparksql").info("*** My Log ***\t-->\t" + o); }
        public static void main(String[] args) {
            System.setProperty("hadoop.home.dir", "c:/hadoop");
            Logger.getLogger("org.apache").setLevel(Level.WARN);

            SparkSession sparkSession = SparkSession.builder()
                    .appName("appName").master("local[*]")
                    .config("spark.sql.warehouse.dir", "file:///c:/tmp/")   // required for Windows
                    .getOrCreate();

            StructField[] structFields = new StructField[] {    // table header
                    new StructField("level", DataTypes.StringType, false, Metadata.empty()),
                    new StructField("datetime", DataTypes.StringType, false, Metadata.empty())
            };
            List<Row> inMemory = new ArrayList<>();
            inMemory.add(RowFactory.create("WARN", "2016-12-31 04:19:32"));
            inMemory.add(RowFactory.create("FATAL", "2016-12-31 03:22:34"));
            inMemory.add(RowFactory.create("WARN", "2016-12-31 03:21:21"));
            inMemory.add(RowFactory.create("INFO", "2015-4-21 14:32:21"));
            inMemory.add(RowFactory.create("FATAL","2015-4-21 19:23:20"));
            StructType schema = new StructType(structFields);
            Dataset<Row> datasetFrame = sparkSession.createDataFrame(inMemory, schema);

            datasetFrame.createOrReplaceTempView("logging_table");
            /** SimpleDateFormat.java */
            Dataset<Row> result = sparkSession.sql(
                    "SELECT level, date_format(datetime, 'MMMM') AS month " +
                            "FROM logging_table "
            );
            result.createOrReplaceTempView("logging_table");
            sparkSession.sql(
                    "SELECT level, month, count(1) " +  // like map/reduce: add a col with val 1 & sum(val)
                            "FROM logging_table " +
                            "group by level, month").show();

            result.show();

            sparkSession.close();
        }
}
