package com.module2.sparkSQL;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;

import static org.apache.spark.sql.functions.*;

public class MainDataFramesAPI {
    static void logInfo(Object o) { Logger.getLogger("com.sparksql").info("*** My Log ***\t-->\t" + o); }
    private enum CE {MONTH, DATETIME, MONTH_NUMBER, LEVEL}
    public static void main(String[] args) {
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        System.setProperty("hadoop.home.dir", "c:/hadoop");
        SparkSession sparkSession = SparkSession.builder()
                .appName("appName").master("local[*]")
                .config("spark.sql.warehouse.dir", "file:///c:/tmp/")   // required for Windows
                .getOrCreate();

        Dataset<Row> dataset = sparkSession.read()
                .option("header", true).csv("src/main/resources/bigLog.txt");

        dataset
            .select(
                col(CE.LEVEL.name()),
                date_format( col(CE.DATETIME.name()), "MMMM").alias(CE.MONTH.name()),
                date_format( col(CE.DATETIME.name()), "M").alias(CE.MONTH_NUMBER.name()).cast(DataTypes.IntegerType)
            )
            .groupBy(
                col(CE.LEVEL.name()),
                col(CE.MONTH.name()),
                col(CE.MONTH_NUMBER.name())
            )
            .count()
            .orderBy(
                    col(CE.MONTH_NUMBER.name()),
                    col("count").desc()
            )
            .drop(CE.MONTH_NUMBER.name())
        .show(1000);


        sparkSession.close();
    }
}
