package com.module2.sparkSQL;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;

import java.util.Arrays;
import java.util.List;

import static com.module1.introduction.bootstrap.LogColumnNames.*;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.date_format;

public class MainPivotTable {
    static void logInfo(Object o) { Logger.getLogger("com.sparksql").info("*** My Log ***\t-->\t" + o); }

    public static void main(String[] args) {
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        System.setProperty("hadoop.home.dir", "c:/hadoop");
        SparkSession sparkSession = SparkSession.builder()
                .appName("appName").master("local[*]")
                .config("spark.sql.warehouse.dir", "file:///c:/tmp/")   // required for Windows
                .getOrCreate();

        Dataset<Row> dataset = sparkSession.read()
                .option("header", true).csv("src/main/resources/bigLog.txt");

        Object[] months = new Object[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "Rocktober", "October", "November", "December"};
        List<Object> columns = Arrays.asList(months);

        dataset = dataset.select(
            col(LEVEL.name()),
            date_format( col(DATETIME.name()), "MMMM").alias(MONTH.name()),
            date_format( col(DATETIME.name()), "M").alias(MONTH_NUMBER.name()).cast(DataTypes.IntegerType)
        );
        // first groupBy rows then pivot(column)
        dataset = dataset.groupBy(LEVEL.name())
                .pivot(MONTH.name(), columns)
                .count()
                .na()               // non-available
                    .fill(69)    // fill in blanks with any value we choose
        ;

        dataset.show(10);

        sparkSession.close();
    }
}
