package com.exercise.one;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;

import scala.Tuple2;

public class ViewingFigures {
	public static void main(String[] args) {

		// system/logger props
		System.setProperty("hadoop.home.dir", "c:/hadoop");
		Logger.getLogger("org.apache").setLevel(Level.WARN);
		// setup-boiler
		SparkConf conf = new SparkConf().setAppName("startingSpark").setMaster("local[*]");
		JavaSparkContext sc = new JavaSparkContext(conf);
		// Use true to use hardcoded data identical to that in the PDF guide.
		boolean testMode = false;

		// given
		JavaPairRDD<Integer, Integer> viewData = setUpViewDataRdd(sc, testMode);
		JavaPairRDD<Integer, Integer> chapterData = setUpChapterDataRdd(sc, testMode);
		JavaPairRDD<Integer, String> titlesData = setUpTitlesDataRdd(sc, testMode);

		// EX_1 - build RDD containing key of courseID with the number of chapters on that course
		JavaPairRDD<Integer, Integer> numberOfChaptersByCourseIdRDD = chapterData
				.mapToPair(t -> new Tuple2<>(t._2, t._1))	// CourseID, ChapterID
				.mapToPair(t -> new Tuple2<>(t._1, 1))		// CourseID, 1 (to be accumulate)
				.reduceByKey(Integer::sum);					// CourseID, NumberOfChapters

		viewData = viewData.distinct();

		JavaPairRDD<Integer, Integer> switchKeyValue = viewData.mapToPair(t -> new Tuple2<>(t._2, t._1));// switch key to value so we can merge with ChapterData
		JavaPairRDD<Integer, Tuple2<Integer, Integer>> leftOuterJoinViewAndChapterDataRDD = switchKeyValue.join(chapterData); // ChapterID, (UserID, CourseID)
		leftOuterJoinViewAndChapterDataRDD.foreach(ViewingFigures::logInfo);
		// We don't need chapterID anymore, because we have the courseID + each dataset record is a distinct view
		// we can flip K,V, map the value from chapterID to integer 1 & reduce by sum
		// compare SUM_VALUE to chaptersPerCourseRDD & calculate SCORE_VALUE (the initial source was ViewData, so we're good)
		// we now have UserID, CourseID, SCORE_VALUE -> we can get rid of userID & reduce(Integer::sum) on SCORE_VALUE

		JavaPairRDD<Tuple2<Integer, Integer>, Integer> chaptersViewedByUserIdByCourseIdRDD = leftOuterJoinViewAndChapterDataRDD.mapToPair(v1 -> {
			Integer userId = v1._2._1;
			Integer courseId = v1._2._2;
			return new Tuple2<>(new Tuple2<>(userId, courseId), 1);   //  ( (userId , courseId) , counter )
		}).reduceByKey(Integer::sum);

		JavaPairRDD<Integer, Integer> removeUserIdRDD = chaptersViewedByUserIdByCourseIdRDD.mapToPair(v1 -> {
			Integer courseId = v1._1._2;
			Integer countersSum = v1._2;
			return new Tuple2<>(courseId, countersSum);
		});
		JavaPairRDD<Integer, Tuple2<Integer, Integer>> joinNumberOfChaptersPerCourse =
				removeUserIdRDD.join(numberOfChaptersByCourseIdRDD);  //  ( courseId, ( viewedChapters, totalChapters ))
		joinNumberOfChaptersPerCourse.foreach(ViewingFigures::logInfo);
//		JavaRDD<Tuple2<Integer, Integer>> calculatePercentageCompleted = joinNumberOfChaptersPerCourse.map(v1 -> {
//			Integer courseId = v1._1;
//			Integer viewedChapters = v1._2._1;
//			Integer totalChapters = v1._2._2;
//			Double percentage = Double.valueOf(viewedChapters) / totalChapters * 100;
//			return new Tuple2<>(courseId, percentage.intValue());
//		});
		JavaPairRDD<Integer, Double> calculateRatioCompleted = joinNumberOfChaptersPerCourse.mapValues(v1 -> (double) v1._1 / v1._2);
		calculateRatioCompleted.foreach(ViewingFigures::logInfo);
		JavaPairRDD<Integer, Integer> transformValueToScore = calculateRatioCompleted.mapValues(v1 -> {
			int score = 0;
			if (v1 > 0.9) score = 10;
			else if (v1 > 0.5) score = 4;
			else if (v1 > 0.25) score = 2;
			return score;
		});
		logInfo("RATIO -> SCORE");
		transformValueToScore.foreach(ViewingFigures::logInfo);
		logInfo("REDUCE BY KEY (SUM)");
		transformValueToScore.reduceByKey(Integer::sum)
				.join(titlesData)
				.mapToPair(record -> new Tuple2<>(record._2._1, record._2._2))
				.sortByKey(false).collect()
				.forEach(ViewingFigures::logInfo);

		Scanner scanner = new Scanner(System.in);
		scanner.nextLine();

		sc.close();
	}
	static void logInfo(Object object) { Logger.getLogger("com.joins").info("\t*** MY_LOG ***\t|\t"+object); }

	private static JavaPairRDD<Integer, String> setUpTitlesDataRdd(JavaSparkContext sc, boolean testMode) {

		if (testMode)
		{
			// (chapterId, title)
			List<Tuple2<Integer, String>> rawTitles = new ArrayList<>();
			rawTitles.add(new Tuple2<>(1, "How to find a better job"));
			rawTitles.add(new Tuple2<>(2, "Work faster harder smarter until you drop"));
			rawTitles.add(new Tuple2<>(3, "Content Creation is a Mug's Game"));
			return sc.parallelizePairs(rawTitles);
		}
		return sc.textFile("src/main/resources/viewing figures/titles.csv")
				.mapToPair(commaSeparatedLine -> {
					String[] cols = commaSeparatedLine.split(",");
					return new Tuple2<Integer, String>(new Integer(cols[0]),cols[1]);
				});
	}

	private static JavaPairRDD<Integer, Integer> setUpChapterDataRdd(JavaSparkContext sc, boolean testMode) {

		if (testMode)
		{
			// (chapterId, (courseId, courseTitle))
			List<Tuple2<Integer, Integer>> rawChapterData = new ArrayList<>();
			rawChapterData.add(new Tuple2<>(96,  1));
			rawChapterData.add(new Tuple2<>(97,  1));
			rawChapterData.add(new Tuple2<>(98,  1));
			rawChapterData.add(new Tuple2<>(99,  2));
			rawChapterData.add(new Tuple2<>(100, 3));
			rawChapterData.add(new Tuple2<>(101, 3));
			rawChapterData.add(new Tuple2<>(102, 3));
			rawChapterData.add(new Tuple2<>(103, 3));
			rawChapterData.add(new Tuple2<>(104, 3));
			rawChapterData.add(new Tuple2<>(105, 3));
			rawChapterData.add(new Tuple2<>(106, 3));
			rawChapterData.add(new Tuple2<>(107, 3));
			rawChapterData.add(new Tuple2<>(108, 3));
			rawChapterData.add(new Tuple2<>(109, 3));
			return sc.parallelizePairs(rawChapterData);
		}

		return sc.textFile("src/main/resources/viewing figures/chapters.csv")
				.mapToPair(commaSeparatedLine -> {
					String[] cols = commaSeparatedLine.split(",");
					return new Tuple2<Integer, Integer>(new Integer(cols[0]), new Integer(cols[1]));
				}).cache();
	}

	private static JavaPairRDD<Integer, Integer> setUpViewDataRdd(JavaSparkContext sc, boolean testMode) {

		if (testMode)
		{
			// Chapter views - (userId, chapterId)
			List<Tuple2<Integer, Integer>> rawViewData = new ArrayList<>();
			rawViewData.add(new Tuple2<>(14, 96));
			rawViewData.add(new Tuple2<>(14, 97));
			rawViewData.add(new Tuple2<>(13, 96));
			rawViewData.add(new Tuple2<>(13, 96));
			rawViewData.add(new Tuple2<>(13, 96));
			rawViewData.add(new Tuple2<>(14, 99));
			rawViewData.add(new Tuple2<>(13, 100));
			return  sc.parallelizePairs(rawViewData);
		}

		return sc.textFile("src/main/resources/viewing figures/views-*.csv")
				.mapToPair(commaSeparatedLine -> {
					String[] columns = commaSeparatedLine.split(",");
					return new Tuple2<Integer, Integer>(new Integer(columns[0]), new Integer(columns[1]));
				});
	}
}