package com.exercise.two;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;

import static org.apache.spark.sql.functions.*;

/** Build a pivot table, showing
 *      - each subject down the "left hand side"
 *      - years across top
 *      - for each subject & year we want:
 *              - avg(exam_score)
 *              - standard deviation of scores      "spread" of values => stddev()
 *              - 2 decimal places
 *      _________________________________________________________________________
 *       subject   | 2001_avg | 2001_stddev | 2002_avg | 2002_stddev | 2003   ...
 *      +------------------------------------------------------------------------
 *       Math      |     1.11 |        2.22 |     3.33 |        4.44 |        ...
 * */
public class Main {
    public static void main(String[] args) {
        System.setProperty("hadoop.home.dir", "c:/hadoop");
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        SparkSession sparkSession = SparkSession.builder()
                .appName("session_ex_2").master("local[*]")
                .config("spark.sql.warehouse.dir", "file:///c:/tmp/")
                .getOrCreate();
        Dataset<Row> dataset = sparkSession.read()
                .option("header", true)
                .csv("src/main/resources/exams/students.csv");
        /** dataset.show();
         * +----------+--------------+----------+----+-------+-----+-----+
         * |student_id|exam_center_id|   subject|year|quarter|score|grade|
         * +----------+--------------+----------+----+-------+-----+-----+
         * |         1|             1|      Math|2005|      1|   41|    D|
         * |         1|             1|   Spanish|2005|      1|   51|    C|
         * ...
         * */
        Column subjectCol = dataset.col("subject").cast(DataTypes.StringType).alias("subject");
        Column scoreCol = dataset.col("score").cast(DataTypes.IntegerType).alias("score");
        Column yearCol = dataset.col("year").cast(DataTypes.IntegerType).alias("year");

        dataset .groupBy(subjectCol)
                .pivot(yearCol)
                .agg(   round(avg(scoreCol), 2)    .alias("avg"),
                        round(stddev(scoreCol), 2) .alias("stddev")
                )
            .show(50);


        sparkSession.close();
    }
}
